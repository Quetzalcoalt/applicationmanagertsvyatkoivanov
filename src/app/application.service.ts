import { Injectable } from '@angular/core';
import { Application } from './applications/applicationModel';
import { Observable, of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

    constructor() { }

    addApplicationToLocalStorage(app: Application): Observable<boolean>{
        let appArray = JSON.parse(localStorage.getItem('applicationsArray'));
        if(appArray !== null){
            appArray.push(app);
            localStorage.setItem('applicationsArray', JSON.stringify(appArray));
            return of(true);
        }else{
            let newArray = [];
            newArray.push(app);
            localStorage.setItem('applicationsArray', JSON.stringify(newArray));
            return of(true);
        }
    }

    editApplication(app: Application): Observable<boolean>{
        let appArray = JSON.parse(localStorage.getItem('applicationsArray'));
        if(appArray !== null){
            for(let i = 0; i <= appArray.length - 1; i++){
                if(appArray[i].id == app.id){
                    appArray[i] = app;
                    break;
                }
            }
            console.log(appArray)
            localStorage.setItem('applicationsArray', JSON.stringify(appArray));
            return of(true);
        }else{
            return of(false);
        }
    }

    getApplicationsArray(): Observable<Application[]>{
        let appArray: Application[] = JSON.parse(localStorage.getItem('applicationsArray'));
        if(appArray !== null){
            return of(appArray);
        }
        else{
            return of([]);
        }
    }

    getId() : Observable<number>{
        let appArray: Application[] = JSON.parse(localStorage.getItem('applicationsArray'));
        if(appArray != null){
            return of(appArray[appArray.length-1].id);
        }
        return of(1);
    }

    getApplicationWithId(id): Observable<Application>{
        let appArray: Application[] = JSON.parse(localStorage.getItem('applicationsArray'));
        let application;
        if(appArray != null){
            application = appArray.find(x => x.id == id);
            return of(application);
        }
        return of(null);
    }

    removeApplicationWithId(id:number): Observable<any>{
        let appArray: Application[] = JSON.parse(localStorage.getItem('applicationsArray'));
        if(appArray != null){
            appArray = appArray.filter(x => x.id != id);
            localStorage.setItem('applicationsArray', JSON.stringify(appArray));
            return of(appArray);
        }
        return of(false);
    }

    addTestData(): any{
        let appArray: Application[] = JSON.parse(localStorage.getItem('applicationsArray'));
        if(appArray !== null){
            let id1 = appArray[appArray.length-1].id;
            id1++;
            let id2 = id1 + 1
            let id3 = id1 + 2
            let app1 = new Application(
                id1,
                "Ivan Banchev",
                "Ivan@gmail.com",
                22,
                "+359 854 659875",
                "Phone",
                "B2",
                new Date(),
                "Students compile a collection of their texts in a variety of genres over time and choose two pieces to present for summative assessment. In the majority of cases, the work in the student’s collection will arise from normal classwork, as the examples below illustrate. ",
                "The annotations capture insights by the student’s teacher, using the features of quality, with a view to establishing the level of achievement the text reflects. The purpose of the annotations is to make the teacher's thinking visible. The annotations were confirmed by the Quality Assurance group, consisting of practicing English teachers and representatives of the Inspectorate, the SEC and JCT. ",
                false,
            );
            let app2 = new Application(
                id2,
                "Ivailo Chalakov",
                "Ivo@gmail.com",
                23,
                "+359 243 654325",
                "Email",
                "C1",
                new Date(),
                "Miss Brill is the story of an old woman told brilliantly and realistically, balancing thoughts and emotions that sustain her late solitary life amidst all the bustle of modern life. Miss Brill is a regular visitor on Sundays to the Jardins Publiques (the Public Gardens) of a small French suburb where she sits and watches all sorts of people come and go.",
                "She listens to the band playing, loves to watch people and guess what keeps them going and enjoys contemplating the world as a great stage upon which actors perform. She finds herself to be another actor among the so many she sees, or at least herself as 'part of the performance after all.'...One Sunday Miss Brill puts on her fur and goes to the Public Gardens as usual. The evening ends with her sudden realization that she is old and lonely, a realization brought to her by a conversation she overhears between a boy and a girl presumably lovers, who comment on her unwelcome presence in their vicinity. ",
                true,
            );
            let app3 = new Application(
                id3,
                "Atanas Tolev",
                "Atanas@gmail.com",
                24,
                "+359 589 548752",
                "Phone",
                "C2",
                new Date(),
                "It's the story of a young prince of Denmark who discovers that his uncle and his mother have killed his father, the former king. He plots to get revenge, but in his obsession with revenge he drives his sweetheart to madness and suicide, kills her innocent father, and in the final scene poisons and is poisoned by her brother in a duel, causes his mother's death, and kills the guilty king, his uncle.",
                "The purpose of a summary is to give a reader a condensed and objective account of the main ideas and features of a text. Usually, a summary has between one and three paragraphs or one hundred to three hundred words, depending on the length and complexity of the original essay and the intended audience and purpose. Typically, a summary will do the following",
                false,
            );
            appArray.push(app1);
            appArray.push(app2);
            appArray.push(app3);
            localStorage.setItem('applicationsArray', JSON.stringify(appArray));
            return of(true);
        }else{
            let newArray = [];
            let app1 = new Application(
                1,
                "Ivan Banchev",
                "Ivan@gmail.com",
                22,
                "+359 854 659875",
                "Phone",
                "B2",
                new Date(),
                "",
                "",
                false,
            );
            newArray.push(app1);
            localStorage.setItem('applicationsArray', JSON.stringify(newArray));
            return of(true);
        }
    }
}
