import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-dialog-delete-application',
  templateUrl: './dialog-delete-application.component.html',
  styleUrls: ['./dialog-delete-application.component.css']
})
export class DialogDeleteApplicationComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<DialogDeleteApplicationComponent>,) { }

    ngOnInit() {
    }

    onNoClick(){
        this.dialogRef.close(false);
    }

    onYesClick(){
        this.dialogRef.close(true);
    }

}
