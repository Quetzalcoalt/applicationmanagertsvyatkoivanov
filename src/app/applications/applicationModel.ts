export class Application {
    id: number;
    name: string;
    email: string;
    age: number;
    phoneNumber: string;
    prefWayOfCommunication: string; 
    englishLevel: string;
    avaiableToStartDate: Date;
    technicalSkillsAndCourses: string;
    shortPersonalPresentation: string;
    studyFromHome: boolean;

    constructor(
        id:number,
        name:string,    
        email: string,
        age: number,
        phoneNumber: string,
        prefWayOfCommunication: string,
        englishLevel: string,
        avaiableToStartDate: Date,
        technicalSkillsAndCourses: string,
        shortPersonalPresentation: string,
        studyFromHome: boolean){
            this.id = id;
            this.name = name;
            this.email = email;
            this.age = age;
            this.phoneNumber = phoneNumber;
            this.prefWayOfCommunication = prefWayOfCommunication;
            this.englishLevel = englishLevel;
            this.avaiableToStartDate = avaiableToStartDate;
            this.technicalSkillsAndCourses = technicalSkillsAndCourses;
            this.shortPersonalPresentation = shortPersonalPresentation;
            this.studyFromHome = studyFromHome;
    }
}