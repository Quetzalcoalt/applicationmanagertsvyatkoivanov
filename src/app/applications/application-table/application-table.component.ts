import { Component, OnInit, ViewChild } from '@angular/core';
import { ApplicationService } from 'src/app/application.service';
import { Application } from 'src/app/applications/applicationModel';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { DialogDeleteApplicationComponent } from 'src/app/Dialogs/dialog-delete-application/dialog-delete-application.component';

@Component({
  selector: 'app-application-table',
  templateUrl: './application-table.component.html',
  styleUrls: ['./application-table.component.css']
})
export class ApplicationTableComponent implements OnInit {

    dataSource = new MatTableDataSource<Application>();
    displayedColumns: string[] = [
        'name',
        'email',
        'phoneNumber',
        'actions',
    ];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public router: Router,
        private applicationService: ApplicationService,
        public dialog: MatDialog) { }

    ngOnInit() {
        this.getApplicationsArray();
    }

    openDialog(id): void {
        const dialogRef = this.dialog.open(DialogDeleteApplicationComponent, {
            autoFocus: false
        });

        dialogRef.afterClosed().subscribe(result => {
            if(result){
                this.removeApplicationWithId(id);
            }
        });
    }

    getApplicationsArray(){
        this.applicationService.getApplicationsArray().subscribe(data => {
            this.initTableData(data)
        })
    }

    removeApplicationWithId(id){
        this.applicationService.removeApplicationWithId(id).subscribe(data => {
            if(data){
                this.initTableData(data)
            }else{
                console.log("item not deleted");
            }
        })
    }

    searchByName(filterValue: string){
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
        }
    }

    initTableData(data){
        this.dataSource = new MatTableDataSource(data)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }
}
