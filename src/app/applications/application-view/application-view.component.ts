import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Application } from '../applicationModel';
import { ApplicationService } from 'src/app/application.service';
import { DialogDeleteApplicationComponent } from 'src/app/Dialogs/dialog-delete-application/dialog-delete-application.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-application-view',
  templateUrl: './application-view.component.html',
  styleUrls: ['./application-view.component.css']
})
export class ApplicationViewComponent implements OnInit {

    id: string;
    application: Application;

    constructor(private activatedRoute: ActivatedRoute,
        private router: Router,
        private applicationService: ApplicationService,
        public dialog: MatDialog) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
            this.getApplicationWithId(this.id)
        })  
    }

    getApplicationWithId(id){
        this.applicationService.getApplicationWithId(id).subscribe(data =>{
            if(data != null){
                this.application = data;
            }else{
                this.router.navigate(['/page404'])
            }
        })
    }

    removeApplicationWithId(id){
        this.applicationService.removeApplicationWithId(id).subscribe(data => {
            if(data){
                this.router.navigate([''])
            }else{
                console.log("item not deleted");
            }
        })
    }

    openDialog(id): void {
        const dialogRef = this.dialog.open(DialogDeleteApplicationComponent, {
            autoFocus: false
        });

        dialogRef.afterClosed().subscribe(result => {
            if(result){
                this.removeApplicationWithId(id);
            }
        });
    }

    routerToEditApplication(){
        this.router.navigate(['/application/edit/' + this.application.id])
    }
}
