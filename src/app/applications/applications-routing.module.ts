import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationsFormComponent } from './applications-form/applications-form.component';
import { ApplicationViewComponent } from './application-view/application-view.component';

const routes: Routes = [
    {
        path: 'view/:id',
        component: ApplicationViewComponent
    },
    {
        path: 'add',
        pathMatch: 'full',
        component: ApplicationsFormComponent
    },
    {
        path: 'edit/:id',
        component: ApplicationsFormComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
