import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

import { Application } from '../applicationModel'
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationService } from 'src/app/application.service';

@Component({
  selector: 'app-applications-form',
  templateUrl: './applications-form.component.html',
  styleUrls: ['./applications-form.component.css']
})
export class ApplicationsFormComponent implements OnInit {

    applicationForm: FormGroup;
    ageList: number[];
    englishLevelList: string[] = ['A2','B1','B2','C1','C2'];
    todaysDate: Date = new Date();
    isLoading: boolean = false;
    applicaitonError: boolean = false;
    edit: boolean = false;
    editId: number;

    constructor(public router: Router, 
        private applicationService: ApplicationService,
        private activatedRoute: ActivatedRoute){}

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.applicationForm = new FormGroup({
                'name' : new FormControl('',[
                    Validators.required,
                    Validators.pattern('[A-Z]{1}[a-z]{2,20}[ ]{1}[A-Z]{1}[a-z]{2,20}')
                ]),
                'email' : new FormControl('', [
                    Validators.required,
                    Validators.email,
                ]),
                'age' : new FormControl('', [
                    Validators.required,
                ]),
                'phoneNumber' : new FormControl('', [
                    Validators.required,
                    Validators.pattern('^[+]359[ ]{1}[0-9]{3}[ ]{1}[0-9]{6}$') //should match only numbers with length of 9
                ]),
                'prefWayOfCommunication' : new FormControl('',[
                    Validators.required,
                ]),
                'englishLevel' : new FormControl('', [
                    Validators.required,
                ]),
                'avaiableToStartDate' : new FormControl('',[
                    Validators.required,
                ]),
                'technicalSkillsAndCourses' : new FormControl(''),
                'shortPersonalPresentation' : new FormControl(''),
                'studyFromHome' : new FormControl(''),
            })
            if(params['id']){
                this.applicationService.getApplicationWithId(params['id']).subscribe(data => {
                    if(data){
                        this.initFilledFormGroup(data);
                        this.editId = data.id;
                        this.edit = true;
                    }else{
                        this.router.navigate(['/page404'])
                    }
                });
            }
        })
        this.ageList = [];
        for(let i = 13; i <= 100; i++){
            this.ageList.push(i);
        }
    }

    initFilledFormGroup(data){
        this.applicationForm = new FormGroup({
            'name' : new FormControl(data.name,[
                Validators.required,
                Validators.pattern('[A-Z]{1}[a-z]{2,20}[ ]{1}[A-Z]{1}[a-z]{2,20}')
            ]),
            'email' : new FormControl(data.email, [
                Validators.required,
                Validators.email,
            ]),
            'age' : new FormControl(data.age, [
                Validators.required,
            ]),
            'phoneNumber' : new FormControl(data.phoneNumber, [
                Validators.required,
                //Validators.pattern('^\+359\s{1}\d{3}\s{1}\d{6}') //should match only numbers with length of 9
            ]),
            'prefWayOfCommunication' : new FormControl(data.prefWayOfCommunication,[
                Validators.required,
            ]),
            'englishLevel' : new FormControl(data.englishLevel, [
                Validators.required,
            ]),
            'avaiableToStartDate' : new FormControl(data.avaiableToStartDate,[
                Validators.required,
            ]),
            'technicalSkillsAndCourses' : new FormControl(data.technicalSkillsAndCourses),
            'shortPersonalPresentation' : new FormControl(data.shortPersonalPresentation),
            'studyFromHome' : new FormControl(data.studyFromHome),
        })
    }

    getNameErrorMessage(){
        if(this.applicationForm.get('name').hasError('required')){
            return 'Name is required.'
        }else if(this.applicationForm.get('name').hasError('pattern')){
            return 'Name must include first and last name with first capital letter';
        }else{
            return '';
        }
    }

    getEmailErrorMessage(){
        if(this.applicationForm.get('email').hasError('required')){
            return 'Email is required.'
        }else if(this.applicationForm.get('email').hasError('email')){
            return 'Please write a valid email.'
        }else{
            return '';
        }
    }

    getPhoneNumberErrorMessage(){
        if(this.applicationForm.get('phoneNumber').hasError('required')){
            return 'Phone number is required.'
        }else if(this.applicationForm.get('phoneNumber').hasError('pattern')){
            return 'Phone number must follow the rule +359 888 888888'
        }else{
            return '';
        }
    }

    getRadioButtonErrorMessage(){
        if(this.applicationForm.get('prefWayOfCommunication').hasError('required')){
            return 'Prefered way of comunication must be checked.'
        }else{
            return '';
        }
    }

    getAgeErrorMessage(){
        if(this.applicationForm.get('age').hasError('required')){
            return 'Age is required.'
        }else{
            return '';
        }
    }

    getEnglishLevelErrorMessage(){
        if(this.applicationForm.get('englishLevel').hasError('required')){
            return 'Enlgish Level is required'
        }else{
            return '';
        }
    }

    getAvaiableToStartDateErrorMessage(){
        if(this.applicationForm.get('avaiableToStartDate').hasError('required')){
            return 'Must select a date.'
        }else{
            return '';
        }
    }

    onSubmit() {
        this.isLoading = true;
        this.applicationService.getId().subscribe(id => {
            id++;
            if(this.edit){
                id = this.editId;
            }
            let application = new Application(
                id,
                this.applicationForm.get('name').value,
                this.applicationForm.get('email').value,
                this.applicationForm.get('age').value,
                this.applicationForm.get('phoneNumber').value,
                this.applicationForm.get('prefWayOfCommunication').value,
                this.applicationForm.get('englishLevel').value,
                this.applicationForm.get('avaiableToStartDate').value,
                this.applicationForm.get('technicalSkillsAndCourses').value,
                this.applicationForm.get('shortPersonalPresentation').value,
                this.applicationForm.get('studyFromHome').value,
            );
            this.applicationForm.reset();
            if(this.edit){
                this.editApplication(application);
            }else{
                this.addNewApplication(application);
            }
        })
    }

    editApplication(application){
        this.applicationService.editApplication(application).subscribe(success => {
            if(success){
                this.isLoading = false;
                this.router.navigate(['']);
            }else{
                //error
                console.log("error in editing application to local storage");
            }
        });
    }

    addNewApplication(application){
        this.applicationService.addApplicationToLocalStorage(application).subscribe(success => {
            if(success){
                this.isLoading = false;
                this.router.navigate(['']);
            }else{
                //error
                console.log("error in adding application to local storage");
            }
        });
    }

    addTestData(){
        this.applicationService.addTestData();
    }
}