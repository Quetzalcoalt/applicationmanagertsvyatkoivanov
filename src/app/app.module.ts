import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DialogDeleteApplicationComponent } from './Dialogs/dialog-delete-application/dialog-delete-application.component';

//Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
import { Page404Component } from './page404/page404.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogDeleteApplicationComponent,
    Page404Component,
  ],
  imports: [
    BrowserModule,

    //Material
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDialogModule,
    MatListModule,

    AppRoutingModule,
  ],
  entryComponents: [
    DialogDeleteApplicationComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
