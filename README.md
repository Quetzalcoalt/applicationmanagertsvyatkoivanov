# ApplicationManager

## Setup Guide

### Step 1

Install [Node.js](https://nodejs.org/en/download/) if you don't have it. This project was done on Node V11.6.0, Windows installer can be found [here](https://nodejs.org/download/release/v11.6.0/node-v11.6.0-x86.msi)

### Step 2

Install [git](https://git-scm.com/downloads) if you don't have it.

### Step 3

Navigate to a desired folder, right click inside it and open `Git Bash Here` <br />
Then run `git clone git@gitlab.com:Quetzalcoalt/applicationmanagertsvyatkoivanov.git`.
This will download the repository. <br />
After everything is downloaded, go inside the generated folder by running `cd applicationmanagertsvyatkoivanov` <br />
Run `npm install`. This will install all Node modules.<br />

### Step 4

Run the following commands one by one: <br />
`npm i -g @angular/cli@7.3.3 `<br />
`npm i -g @angular/material@7.3.3` <br />
`npm i -g @angular/cdk@7.3.3` <br />
`npm i -g @angular/animations@7.2.6` <br />


### Step 5

Run `ng serve` to start a local dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.